---
author: Laura Fléron
title: 📖 Ep 30

tags: 
 - _2023
---

▶ Télécharger [**le sujet en pdf**](23-NSI-30.pdf).

## EXERCICE 1

Écrire une fonction `moyenne` qui prend en paramètre un tableau non vide de nombres flottants et qui renvoie la moyenne des valeurs du tableau. Les tableaux seront représentés sous forme de liste Python.

Exemples :
```python
    >>> moyenne([1.0])
    1.0
    >>> moyenne([1.0, 2.0, 4.0])
    2.3333333333333335
```

??? Example "Réponse"

    Complétez le code ci-dessous
    
    {{IDE('Exo1Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo1S', SIZE=50)}}

## EXERCICE 2

On considère la fonction binaire ci-dessous qui prend en paramètre un entier positif a en écriture décimale et qui renvoie son écriture binaire sous la forme d'une chaine de caractères.

L’algorithme utilise la méthode des divisions euclidiennes successives comme l’illustre l’exemple ci-après.

![image](divisions.png){: .center}

Compléter le code de la fonction binaire.

??? Example "Réponse"

    Complétez le code ci-dessous

    {{IDE('Exo2Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo2S', SIZE=50)}}

Exemples :
```python
    >>> binaire(83)
    '1010011'
    >>> binaire(127)
    '1111111'
    >>> binaire(0)
    '0'
```
