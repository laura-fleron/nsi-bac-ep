---
author: Laura Fléron
title: 📖 Ep 13

tags:
 - _2023
---

▶ Télécharger [**le sujet en pdf**](23-NSI-13.pdf).

## EXERCICE 1

Écrire en langage Python une fonction `recherche` prenant comme paramètres une variable `a` de type numérique (`float` ou `int`) et un tableau `tab` (type `list`) et qui
renvoie le nombre d'occurrences de `a` dans `tab`.

Exemples :
```python
    >>> recherche(5, [])
    0
    >>> recherche(5, [-2, 3, 4, 8])
    0
    >>> recherche(5, [-2, 3, 1, 5, 3, 7, 4])
    1
    >>> recherche(5, [-2, 5, 3, 5, 4, 5])
    3
```

??? Example "Réponse"

    Complétez le code ci-dessous
    
    {{IDE('Exo1Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo1S', SIZE=50)}}

## EXERCICE 2

La fonction `rendu_monnaie` prend en paramètres deux nombres entiers positifs `somme_due` et `somme_versee` et elle permet de procéder au rendu de monnaie de la
différence `somme_versee – somme_due` pour des achats effectués avec le système de pièces de la zone Euro. On utilise pour cela un algorithme glouton qui commence par rendre le maximum de pièces de plus grandes valeurs et ainsi de suite. Par la suite, on assimilera les billets à des pièces.

La fonction `rendu_monnaie` renvoie un tableau de type `list` contenant les pièces qui composent le rendu.

Toutes les sommes sont exprimées en euros. Les valeurs possibles pour les pièces sont donc `[1, 2, 5, 10, 20, 50, 100, 200]`.

Ainsi, l’instruction `rendu_monnaie(452, 500)` renvoie le tableau `[20, 20, 5, 2, 1]`.

En effet, la somme à rendre est de `48` euros soit `20 + 20 + 5 + 2 + 1`.

Le code de la fonction `rendu_monnaie` est donné ci-dessous :

Compléter ce code et le tester :

```python
    >>> rendu_monnaie(700,700)
    []
    >>> rendu_monnaie(102,500)
    [200, 100, 50, 20, 20, 5, 2, 1]
```

??? Example "Réponse"

    Complétez le code ci-dessous

    {{IDE('Exo2Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo2S', SIZE=50)}}