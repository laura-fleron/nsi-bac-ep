class Pile:
    """Classe définissant une structure de pile."""
    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon."""
        return self.contenu == []

    def empiler(self, v):
        """Place l'élément v au sommet de la pile"""
        self.contenu.append(v)

    def depiler(self):
        """
        Retire et renvoie l’élément placé au sommet de la pile,
        si la pile n’est pas vide.
        """
        if not self.est_vide():
            return self.contenu.pop()

def eval_expression(tab):
    p = Pile()
    for element in tab:
        if element != '+' and element != '*':
            p.empiler(element)
        else:
            if element == '+':
                resultat = p.depiler() + p.depiler()
            else:
                resultat = p.depiler() * p.depiler()
            p.empiler(resultat)
    return p.depiler()

# Testez votre algorithme
try:
    assert eval_expression([2, 3, '+', 5, '*']) == 25
    assert eval_expression([4, 4, '+', 3, '*']) == 24
    assert eval_expression([4, 2, '+', 3, '*']) == 18
    print('Tout semble correct 👍')
    
except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
