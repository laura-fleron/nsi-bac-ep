## EXERCICE 1

??? Example "Python"

    === "Répondre à la question ici"
        Complétez le code ci-dessous, si le code ne s'affiche pas cliquez sur le bouton **Recharger**
        {{IDE('scripts/Exo1Q', MAX = 1000)}}

    === "Solution"
        Si le code ne s'affiche pas cliquez sur le bouton **Recharger**
        {{IDE('scripts/Exo1S', MAX = 1000)}}

## EXERCICE 2

??? Example "Python"

    === "Répondre à la question ici"
        Complétez le code ci-dessous, si le code ne s'affiche pas cliquez sur le bouton **Recharger**
        {{IDE('scripts/Exo2Q', MAX = 1000)}}

    === "Solution"
        Si le code ne s'affiche pas cliquez sur le bouton **Recharger**
        {{IDE('scripts/Exo2S', MAX = 1000)}}
