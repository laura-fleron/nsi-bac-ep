from math import sqrt     # import de la fonction racine carrée

def distance(point1, point2):
    """ Calcule et renvoie la distance entre deux points. """
    return sqrt((...)**2 + (...)**2)

def plus_courte_distance(tab, depart):
    """ Renvoie le point du tableau tab se trouvant à la plus courte distance du point depart."""
    point = tab[0]
    min_dist = ...
    for i in range (1, ...):
        if distance(tab[i], depart)...:
            point = ...
            min_dist = ...
    return point

if __name__ == "__main__":

    try:
        assert distance((1, 0), (5, 3)) == 5
        assert 1.414 < distance((1, 0), (0, 1)) < 1.415
        assert plus_courte_distance([(7, 9), (2, 5), (5, 2)], (0, 0)) == (2, 5)
        assert plus_courte_distance([(7, 9), (2, 5), (5, 2)], (5, 2)) == (5, 2)
        print('Tout semble correct 👍')
        
    except AssertionError as asser:
        print('Le résultat de votre fonction n\'est pas conforme 🤔')

