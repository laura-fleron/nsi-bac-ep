---
author: Laura Fléron
title: 📖 Ep 09

tags:
 - _2023
---

▶ Télécharger [**le sujet en pdf**](>23-NSI-09.pdf).

## EXERCICE 1

Programmer la fonction `multiplication`, prenant en paramètres deux nombres entiers `n1` et `n2`, et qui renvoie le produit de ces deux nombres.

Les seules opérations autorisées sont l’addition et la soustraction.

Exemples :
```python
    >>> multiplication(3,5)
    15
    >>> multiplication(-4,-8)
    32
    >>> multiplication(-2,6)
    -12
    >>> multiplication(-2,0)
    0
```

??? Example "Réponse"

    Complétez le code ci-dessous
    
    {{IDE('Exo1Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo1S', SIZE=50)}}

## EXERCICE 2

Soit `tab` un tableau non vide d'entiers triés dans l'ordre croissant et `n` un entier.

La fonction `chercher` ci-dessous doit renvoyer un indice où la valeur `n` apparaît dans `tab` si cette valeur y figure et `None` sinon. 

Les paramètres de la fonction sont :

- `tab`, le tableau dans lequel s'effectue la recherche ;
- `n`, l'entier à chercher dans le tableau ;
- `i`, l'indice de début de la partie du tableau où s'effectue la recherche ;
- `j`, l'indice de fin de la partie du tableau où s'effectue la recherche.

L’algorithme demandé est une recherche dichotomique récursive.

Recopier et compléter le code de la fonction `chercher` suivante :

L'exécution du code doit donner :

```python 
    >>> chercher([1, 5, 6, 6, 9, 12], 7, 0, 10)
    >>> chercher([1, 5, 6, 6, 9, 12], 7, 0, 5)
    >>> chercher([1, 5, 6, 6, 9, 12], 9, 0, 5)
    4
    >>> chercher([1, 5, 6, 6, 9, 12], 6, 0, 5)
    2
```

??? Example "Réponse"

    Complétez le code ci-dessous

    {{IDE('Exo2Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo2S', SIZE=50)}}