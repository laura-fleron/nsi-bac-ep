---
author: Laura Fléron
title: 📖 Ep 21

tags: 
 - _2023
---

▶ Télécharger [**le sujet en pdf**](23-NSI-21.pdf).

## EXERCICE 1

Le codage par différence (*delta encoding* en anglais) permet de compresser un tableau dedonnées en indiquant pour chaque donnée, sa différence avec la précédente (plutôt que la
donnée elle-même). On se retrouve alors avec un tableau de données plus petit, nécessitant moins de place en mémoire. Cette méthode se révèle efficace lorsque les valeurs consécutives
sont proches.

Programmer la fonction `delta(liste)` qui prend en paramètre un tableau non vide de nombres entiers et qui renvoie un tableau contenant les valeurs entières compressées à l’aide cette technique.

Exemples :

```python
    >>> delta([1000, 800, 802, 1000, 1003])
    [1000, -200, 2, 198, 3]
    >>> delta([42])
    [42] 
```

??? Example "Réponse"

    Complétez le code ci-dessous
    
    {{IDE('Exo1Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo1S', SIZE=50)}}

## EXERCICE 2

Une expression arithmétique ne comportant que les quatre opérations +, −, ×, ÷ peut être représentée sous forme d’arbre binaire. Les nœuds internes sont des opérateurs et les feuilles
sont des nombres. Dans un tel arbre, la disposition des nœuds joue le rôle des parenthèses que nous connaissons bien.  

![image](arbre.png){: .center width=30%}

En parcourant en profondeur infixe l’arbre binaire ci-dessus, on retrouve l’expression notée habituellement :  

$$(3 \times (8 + 7)) − (2 + 1)$$

La classe `Noeud` ci-après permet d’implémenter une structure d’arbre binaire.

Compléter la fonction récursive `expression_infixe` qui prend en paramètre un objet de la classe `Noeud` et qui renvoie l’expression arithmétique représentée par l’arbre binaire passé en paramètre, sous forme d’une chaîne de caractères contenant des parenthèses.  

Résultat attendu avec l’arbre ci-dessus :

```python
    >>> e = Noeud(Noeud(Noeud(None, 3, None), '*', Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))), '-', Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None)))
    >>> expression_infixe(e)
    '((3*(8+7))-(2+1))'
```

??? Example "Réponse"

    Complétez le code ci-dessous

    {{IDE('Exo2Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo2S', SIZE=50)}}