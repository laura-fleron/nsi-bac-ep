---
author: Laura Fléron
title: A propos

hide:
  - tags
---

Laura Fléron, NSI, Académie de Reims.

## Crédits

Le site est hébergé par la forge de [*l'Association des Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/).

Le site est construit avec [mkdocs](https://www.mkdocs.org/) et en particulier [mkdocs-material](https://squidfunk.github.io/mkdocs-material/).

Modèle avec Pyoide basé sur le travail de vincent_bouillot [Outils NSI](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/).


Un repo où les élèves peuvent s’entrainer sur [CodEX](https://codex.forge.apps.education.fr/) reprenant et complétant la base des exercices présents dans le projet e-nsi. Le contenu est rédigé et relu entre pairs. Les exercices sont réalisables directement dans le navigateur, sans aucun installation ni inscription.

## Version

|Date       |Objet                      |
|:---------:|:-------------------------:|
|25/03/2024 |Migration vers NSI-BAC-EP  |
|23/03/2024 |Ep-2024 en production      |
|12/03/2024 |importation des EP 2024    |
|31/01/2024 |cor.htm,diff.htm:Datatable |
|09/05/2023 |reset commits -> Ver 1.0   |
|07/05/2023 |pyoide size=50 ds les ep   |
|06/05/2023 |Ep dans chaque dossier     |
|04/05/2023 |Pyoide pour toutes les EP  |
|25/04/2023 |Maj Pyoide 0.9.1           |
|18/04/2023 |IDE intégrée en dev        |
|16/04/2023 |+ Difficulté des sujets    |
|14/04/2023 |+ Fichier corresp          |
|13/04/2023 |Squelette, tags, pdf ->20  |

Vers mon Repository : [nsi_ep2023](https://forge.aeif.fr/lfleron/nsi_ep2023/)