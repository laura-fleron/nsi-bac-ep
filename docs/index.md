---
author: Laura Fléron
title: Index
---
Ce *repo* recense les épreuves pratiques année par année. **Il n'a pas vocation a être employé avec les élèves en classe** mais peut servir d'inspiration pour les enseignants en informatique.

Les solutions fournies :

- ne sont pas officielles, 
- peuvent comporter des erreurs,
- ne sont pas uniques,

Les valeurs ajoutées de ce *repo* sont :

- Une recherche par tags et une page de recherche sous forme d'un tableau
- Une estimation du niveau de difficulté des sujets triés par tags

[Les sources relatives à la session 2024 sont disponibles dans la banque nationale de sujets](https://cyclades.education.gouv.fr/delos/public/listPublicECE)

#### Information sur le site : ⚙️ En production

## Vous recherchez une épreuve en fonction d'un thème ?

??? tip "Astuce"
        
    Les épreuves sont classées par *tags*, ce qui vous permet de choisir un thème particulier.

    Pour cela rendez-vous dans la section [**Catégories**](tags.md), choisissez un thème puis une épreuve.

    Une fois sur l'épreuve, vous pouvez revenir sur le thème en cliquant sur l'étiquette en haut de l'épreuve et passer ainsi sur une autre épreuve du même thème.

## Le site sur votre smartphone

??? success "Qr-code vers le site"

    ![image](qr-code.png){: .center width=30%}
