---
author: Laura Fléron
title: Correspondance

hide:
  - tags
---

Accès vers les données croisées :
<ul>
<li><a href="../cor.html" title="correspondance" target="_blank">Les sujets de l'année en cours et des années antérieures.</a></li>
<li><a href="../difficulte.html" title="difficulté" target="_blank">Notion sur la difficulté des sujets</a></li>
</ul>

<!--
### Dessous obsolète...
*Les données ont été importées et n'ont pas été (toutes) vérifiées.*

|2023(*)|Exercice 1|Exercice 2|2022(*)|2021|
|:-------:|:-------:|:-------:|:-------:|:-------:|
|1|Vérification si une liste est triée ou non|Comptabilisation de votes (dictionnaires)|37(*)||
|2|Maximum et indice d'un élément dans une liste|Structure de données : piles|16|10|
|3|Calcul d'une moyenne pondérée|Représentation d'une image par une liste de liste|39|27|
|4|Recherche de doublon dans une liste|Démineur matrice| |14|
|5|Paire de 6|Représentation d'une image par une liste de listes| ||
|6(*)|Dernière occurrence d'un élément dans une liste|Calcul de la distance entre deux points|36(*)|1|
|7|Fusion de deux listes déjà triées|Conversion numération romaine|30||
|8(*)|Maximum des valeurs d'un dictionnaire|POO : pile pour noter une expression arithmétique|23(*)||
|9|Multiplications avec uniquement additions et soustractions|Recherche dichotomique|19|30|
|10(*)|Maximum des éléments d'une liste|POO : expression bien parenthésée et piles|24||
|11(*)|Conversion binaire décimal|Tri par insertion|33|5|
|12|Ajouter un élément dans un ABR|Algorithme glouton| ||
|13(*)|Nombre d'occurrence d'un élément dans une liste|Rendu de monnaie|31(*)|22|
|14|Recherche de la première occurrence|Insertion dans une liste triée|8|18|
|15(*)|Minimum d'une liste de températures Palindrome|Palindrome|18(*)|20|
|16|Recherche d'indices < = > Moyenne pondérée avec un dictionnaire|Moyenne pondérée avec un dictionnaire| ||
|17|Calcul d'une moyenne|Triangle de Pascal|2|9|
|18|Indice de première apparition du max|Chromosome parcours d'une liste| ||
|19|Recherche dichotomique|Code de César|11|19|
|20(*)|Fusion de dictionnaires|Tirage au sort| ||
|21|Codage par différence|Arbre binaire et expression arithmétique|3||
|22|Suite de Collatz|Codage d'un mot|9|29|
|23|Traitement de données en tables|Recherche récursive dans un tableau|25||
|24|Nombre d'occurrence avec un dictionnaire|Fusion de deux listes triées|10|23|
|25|Occurrence d'un nombre dans une liste|Insertion dans un ABR| ||
|26(*)|Multiplications avec uniquement additions et soustractions|Recherche dichotomique dans un tableau trié|21(*)|3|
|27(*)|Recherche d’un minimum|Tri par sélection|26(*)|17|
|28(*)|Calcul d'une moyenne|Recherche dichotomique|35|4|
|29|Taille et hauteur d'un arbre|Insertion dans une liste triée| ||
|30(*)|Calcul de moyenne|Conversion décimal en binaire|28(*)|16|
|31(*)|Nombre d'occurrence d'un élément dans une liste|Conversion décimal en binaire|15(*)|21|
|32|Recherche du minimum et du maximum|POO : cartes et paquet de cartes|5|15|
|33|Taille dans un arbre binaire|Tri par sélection|27|28|
|34(*)|Calcul d'une moyenne|Séparation des 0 et des 1 dans une liste|12(*)|2|
|35(*)|Ou exclusif entre deux tableaux|POO : Test de carrés magiques|20||
|36|Entiers consécutifs dans un tableau|Codage d'une image en liste de liste|4|25|
|37|Dernière occurrence d'un élément dans une liste|POO : adresse IP|32(*)|24|
|38|Mots correspondants à un motif|Recherche d'un cycle|14||
|39|Termes de la suite de Fibonacci|Recherche de maximum dans une liste|29|7|
|40|Compter le nombre de mots|Insertion d'un nœud dans un ABR|||
|41(*)|Recherche d'occurrences|Rendu de monnaie récursif|1|8|
|42|Tri par sélection|Jeu du nombre mystère|38|13|
|43|Conversion binaire/décimal|Tri à bulles|7|11|
|44|Écriture d'une chaîne de caractères à l'envers|Crible d'Eratosthène|22||
|45|Tri et occurrence d'une liste|Convertir bin -* Dec et Dec -» Bin|||

20xx(*), le sujet a été sélectionné.
-->
