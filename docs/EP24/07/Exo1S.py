def gb_vers_entier(tab):
    puissance = 0
    total = 0
    for i in range(len(tab)-1, -1, -1):
        total += tab[i]*(2**puissance)
        puissance += 1
    return total

try:
    assert gb_vers_entier([]) == 0
    assert gb_vers_entier([True]) == 1
    assert gb_vers_entier([True, False, True, False, False, True, True]) == 83
    assert gb_vers_entier([True, False, False, False, False, False, True, False]) == 130
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')



