def moyenne(tab):
    if tab == []:
        return []
    else:
        somme = 0
        for elt in tab:
            somme += elt
        return somme / len(tab)

try:
    assert round(moyenne([5,3,8]), 3) == 5.333
    assert moyenne([1,2,3,4,5,6,7,8,9,10]) == 5.5
    assert moyenne([]) == []
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')