def nombre_de_mots(phrase):
    nb_mots = 0
    for caractere in phrase:
        if caractere == ' ' or caractere == '.':
            nb_mots += 1
    return nb_mots

try:
    assert nombre_de_mots('Le point d exclamation est separe !') == 6
    assert nombre_de_mots('Il y a un seul espace entre les mots !') == 9
    assert nombre_de_mots('Combien de mots y a t il dans cette phrase ?') == 10
    assert nombre_de_mots('Fin.') == 1
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')