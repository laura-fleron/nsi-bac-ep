class Noeud:
    def __init__(self, etiquette):
        '''Méthode constructeur pour la classe Noeud.
        Crée une feuille d'étiquette donnée.'''
        self.etiquette = etiquette
        self.gauche = None
        self.droit = None

    def inserer(self, cle):
        '''Insère la clé dans l'arbre binaire de recherche
        en préservant sa structure.'''
        if cle < self.etiquette:
            # on insère à gauche
            if self.gauche != None:
                # on descend à gauche et on retente l'insertion de la clé
                self.gauche.inserer(cle)
            else:
                # on crée un fils gauche
                self.gauche = Noeud(cle)
        else:
            # on insère à droite
            if self.droit != None:
                # on descend à droite et on retente l'insertion de la clé
                self.droit.inserer(cle)
            else:
                # on crée un fils droit
                self.droit = Noeud(cle)

try:
    arbre = Noeud(7)
    for cle in (3, 9, 1, 6):
        arbre.inserer(cle)

    assert arbre.gauche.etiquette == 3
    assert arbre.droit.etiquette == 9
    assert arbre.gauche.gauche.etiquette == 1
    assert arbre.gauche.droit.etiquette == 6
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
