def voisins_entrants(adj, x):
    voisin = []
    noeuds = len(adj)
    for i in range(noeuds):
        if x in adj[i]:
            voisin.append(i)
    return voisin

try:
    adj = [[1, 2], [2], [0], [0]]
    assert voisins_entrants([[1, 2], [2], [0], [0]], 0) == [2, 3]
    assert voisins_entrants([[1, 2], [2], [0], [0]], 1) == [0]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')