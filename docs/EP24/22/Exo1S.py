def recherche_indices_classement(elt, tab):
    lst1 = []
    lst2 = []
    lst3 = []

    for i in range(len(tab)):
        if tab[i] < elt:
            lst1.append(i)
        elif tab[i] > elt:
            lst3.append(i)
        else:
            lst2.append(i)
    return lst1, lst2, lst3

try:
    assert recherche_indices_classement(3, [1, 3, 4, 2, 4, 6, 3, 0]) == ([0, 3, 7], [1, 6], [2, 4, 5])
    assert recherche_indices_classement(3, [1, 4, 2, 4, 6, 0]) == ([0, 2, 5], [], [1, 3, 4])
    assert recherche_indices_classement(3, [1, 1, 1, 1]) == ([0, 1, 2, 3], [], [])
    assert recherche_indices_classement(3, []) == ([], [], [])
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')