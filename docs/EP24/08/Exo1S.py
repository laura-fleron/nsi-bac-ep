def delta(tab):
    diff = [tab[0]]
    for i in range(1, len(tab)):
        diff.append(tab[i] - tab[i-1])
    return diff

try:
    assert delta([1000, 800, 802, 1000, 1003]) == [1000, -200, 2, 198, 3]
    assert delta([42]) == [42]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

