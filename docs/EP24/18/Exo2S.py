def chercher(tab, n, i, j):
    if i < 0 or j > len(tab) :
        return None
    if i > j :
        return None
    m = (i + j) // 2
    if tab[m] < n :
        return chercher(tab, n, m+1 , j)
    elif tab[m] > n :
        return chercher(tab, n, i , m-1 )
    else :
        return m

try:
    assert chercher([1, 5, 6, 6, 9, 12], 7, 0, 10) == None
    assert chercher([1, 5, 6, 6, 9, 12], 7, 0, 5) == None
    assert chercher([1, 5, 6, 6, 9, 12], 9, 0, 5) == 4
    assert chercher([1, 5, 6, 6, 9, 12], 6, 0, 5) == 2
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

