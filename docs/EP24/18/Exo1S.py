def multiplication(n1, n2):
    if n1 < 0:
        return -multiplication(-n1, n2)
    if n2 < 0:
        return -multiplication(n1, -n2)
    resultat = 0
    for _ in range(n2):
        resultat += n1
    return resultat

try:
    assert multiplication(3,5) == 15
    assert multiplication(-4,-8) == 32
    assert multiplication(-2,6) == -12
    assert multiplication(-2,0) == 0
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

