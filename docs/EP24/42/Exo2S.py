def dichotomie(tab, x):
    """
    tab : tableau d’entiers trié dans l’ordre croissant
    x : nombre entier
    La fonction renvoie True si tab contient x et False sinon
    """
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = (debut + fin) // 2
        if x == tab[m]:
            return True
        if x > tab[m]:
            debut = m + 1
        else:
            fin = m - 1
    return False

try:
    assert dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 28) == True
    assert dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 27) == False
    assert dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 1) == False
    assert dichotomie([], 28) == False
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')