---
author: Laura Fléron
title: 📖 Ep 42

tags: 
 - Dichotomie
 - Listes Min max Moy Rch
---

[**:material-progress-download: le document**](24-NSI-42.pdf)

[**:material-progress-download: l'algorithme**](24-NSI-42.py)

#### Issue de : 23-NSI-28

## EXERCICE 1

Programmer la fonction `multiplication`, prenant en paramètres deux nombres entiers `n1` et `n2`, et qui renvoie le produit de ces deux nombres.

Écrire une fonction `moyenne` qui prend en paramètre un tableau d’entiers non vide et qui renvoie un nombre flottant donnant la moyenne de ces entiers.

Attention : il est interdit d’utiliser la fonction sum ou la fonction mean (module statistics) de Python

Exemple :
```python
>>> moyenne([1])
1.0
>>> moyenne([1, 2, 3, 4, 5, 6, 7])
4.0
>>. moyenne([1, 2])
1.5
```

??? Example "Réponse"

    Complétez le code ci-dessous
    
    {{IDE('Exo1Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo1S', SIZE=50)}}

## EXERCICE 2

Le but de l’exercice est de compléter une fonction qui détermine si une valeur est présente dans un tableau de valeurs triées dans l’ordre croissant.

Compléter l’algorithme de dichotomie donné ci-après.

```python linenums='1'
def dichotomie(tab, x):
    """
    tab : tableau d'entiers trié dans l'ordre croissant
    x : nombre entier
    La fonction renvoie True si tab contient x et False sinon
    """
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = ... 
        if x == tab[m]:
            return ... 
        if x > tab[m]:
            debut = m + 1
        else:
            fin = ... 
    return ... 
```

Exemples :

```python 
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 28)
True
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 27)
False
>>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 1)
False
>>> dichotomie([], 28)
False
```
??? Example "Réponse"

    Complétez le code ci-dessous

    {{IDE('Exo2Q', SIZE=50)}}

??? Success "Solution"

    {{IDE('Exo2S', SIZE=50)}}
