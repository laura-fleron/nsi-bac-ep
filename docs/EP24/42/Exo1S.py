def moyenne(tab):
    '''
    moyenne(list) -> float
    Entrée : un tableau non vide d'entiers
    Sortie : nombre de type float
    Correspondant à la moyenne des valeurs présentes dans le
    tableau
    '''
    somme = 0
    for elt in tab:
        somme += elt
    return somme / len(tab)
try:
    assert moyenne([1, 2, 3, 4, 5, 6, 7]) == 4.0
    assert moyenne([1, 2]) == 1.5
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
