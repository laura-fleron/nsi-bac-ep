def compte_occurrences(a, tab):
    nb = 0
    for element in tab:
        if element == a:
            nb += 1
    return nb

try:
    assert compte_occurrences(5, []) == 0
    assert compte_occurrences(5, [-2, 3, 1, 5, 3, 7, 4]) == 1
    assert compte_occurrences('a', ['a','b','c','a','d','e','a']) == 3
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')