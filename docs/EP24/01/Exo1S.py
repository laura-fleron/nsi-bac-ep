arbre = {'F':['B','G'], 
         'B':['A','D'], 
         'A':['',''], 
         'D':['C','E'], 
         'C':['',''], 
         'E':['',''], 
         'G':['','I'], 
         'I':['','H'], 
         'H':['','']}

def taille(arbre, lettre):
    if lettre == '':
        return 0
    return 1 + taille(arbre, arbre[lettre][0]) + taille(arbre, arbre[lettre][1])

taille(arbre, 'F')

# Testez votre algorithme
try:
    assert taille(arbre, 'F') == 9
    print('Tout semble correct 👍')
    
except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')