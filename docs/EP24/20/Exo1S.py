from random import randint

def lancer(n):
    return [randint(1,6) for _ in range(n)]

def paire_6(tab):
    nb = 0
    for elt in tab:
        if elt == 6:
            nb += 1
    if nb >=2 :
        return True
    else:
        return False

try:
    assert len(lancer(5)) == 5
    assert len(lancer(3)) == 3
    assert len(lancer(0)) == 0

    lancer1 = [5, 6, 6, 2, 2]
    assert paire_6(lancer1) == True

    lancer2 = [6, 5, 1, 6, 6]
    assert paire_6(lancer2) == True

    lancer3 = [2, 2, 6]
    assert paire_6(lancer3) == False

    lancer4 = []
    assert paire_6(lancer4) == False
    
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')


