---
author: Laura Fléron
title: 📖 Ep__

hide: 
 - Algo Glouton
 - Arbres B. Rech.
 - Assertions
 - Dichotomie
 - Dictionnaires
 - Diviser pour régner
 - KNN
 - Listes Min max Moy Rch
 - Listes opérations
 - Logique Booléene
 - Piles, files
 - Poo
 - Prog. Dynamique
 - Représ des nombres
 - Récursivité
 - Tris
---

[**:material-progress-download: le document**](24-NSI-48.pdf)

[**:material-progress-download: l'algorithme**](24-NSI-48.py)

#### Issue de : 23-NSI-

```python linenums='1'
```
