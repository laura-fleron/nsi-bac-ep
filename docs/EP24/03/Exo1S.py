def maximum_tableau(tab):
    maximum = tab[0]
    for element in tab:
        if element > maximum:
            maximum = element
    return maximum

# Testez votre algorithme
try:
    assert maximum_tableau([98, 12, 104, 23, 131, 9]) == 131
    assert maximum_tableau([-27, 24, -3, 15]) == 24
    print('Tout semble correct 👍')
    
except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')