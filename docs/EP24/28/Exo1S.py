def fibonacci1(n):
    if n == 1 :
        return 1   
    elif n == 1 :
        return 1
    else :
        return fibonacci1(n-1) + fibonacci1(n-2)

# ou

def fibonacci2(n):
    a = 1
    b = 1
    for k in range(n-2):
        t = b
        b = a + b
        a = t
    return b

# ou

def fibonacci(n):
    d = {}
    d[1] = 1
    d[2] = 1
    for k in range(3, n+1):
        d[k] = d[k-1] + d[k-2]
    return d[n]

try:
    assert fibonacci(1) == 1
    assert fibonacci(2) == 1
    assert fibonacci(25) == 75025
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')