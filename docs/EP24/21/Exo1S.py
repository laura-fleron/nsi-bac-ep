def recherche_motif(motif, text):
    """Méthode naïve"""
    lg_text = len(text)
    lg_motif = len(motif)
    position = []
    for i in range(lg_text-lg_motif+1):
        if text[i:i+lg_motif] == motif:
            position.append(i)
    return position
            
try:
    assert recherche_motif("ab", "") == []
    assert recherche_motif("ab", "cdcdcdcd") == []
    assert recherche_motif("ab", "abracadabra") == [0, 7]
    assert recherche_motif("ab", "abracadabraab") == [0, 7, 11]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
