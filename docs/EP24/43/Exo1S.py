def a_doublon(lst):
    for i in range(len(lst)-1):
        if lst[i] == lst[i+1]:
            return True
    return False

try:
    assert a_doublon([]) == False
    assert a_doublon([1]) == False
    assert a_doublon([1, 2, 4, 6, 6]) == True
    assert a_doublon([2, 5, 7, 7, 7, 9]) == True
    assert a_doublon([0, 2, 3]) == False
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')