def max_et_indice(tab):
    '''
    renvoie la valeur du plus grand élément de cette liste ainsi
    que l’indice de sa première apparition dans cette liste.
    '''
    assert tab != [], 'le tableau est vide'

    val_max = tab[0]
    ind_max = 0
    for i in range(len(tab)):
        if tab[i] > val_max:
            val_max = tab[i]
            ind_max = i
    return (val_max, ind_max)

try:
    assert max_et_indice([1, 5, 6, 9, 1, 2, 3, 7, 9, 8]) == (9, 3)
    assert max_et_indice([-2]) == (-2, 0)
    assert max_et_indice([-1, -1, 3, 3, 3]) == (3, 2)
    assert max_et_indice([1, 1, 1, 1]) == (1, 0)
    print('Tout semble correct 👍')
    
except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
