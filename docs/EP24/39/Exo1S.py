def recherche(elt, tab):
    for i in range(len(tab)-1, -1, -1):
        if tab[i] == elt:
            return i
    return None

try:
    assert recherche(1, [2, 3, 4]) == None
    assert recherche(1, [10, 12, 1, 56]) == 2
    assert recherche(1, [1, 0, 42, 7]) == 0
    assert recherche(1, [1, 50, 1]) == 2
    assert recherche(1, [8, 1, 10, 1, 7, 1, 8]) == 5
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')