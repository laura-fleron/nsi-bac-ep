class AdresseIP:
    def __init__(self, adresse):
        self.adresse = adresse

    def liste_octets(self):
        """renvoie une liste de nombres entiers,
        la liste des octets de l'adresse IP"""
        # Note : split découpe la chaine de caractères 
        # en fonction du séparateur
        return [int(i) for i in self.adresse.split(".")]

    def est_reservee(self):
        """renvoie True si l'adresse IP est une adresse
        réservée, False sinon"""
        reservees = self.liste_octets()[3] 
        return reservees == 0 or reservees == 255 
      
    def adresse_suivante(self):
        """renvoie un objet de AdresseIP avec l'adresse
        IP qui suit l'adresse self si elle existe et None sinon"""
        octets = self.liste_octets()[3] 
        if octets == 254: 
            return None
        octet_nouveau = self.liste_octets()[3] + 1 
        return AdresseIP('192.168.0.' + str(octet_nouveau))

try:
    adresse1 = AdresseIP('192.168.0.1')
    adresse2 = AdresseIP('192.168.0.2')
    adresse3 = AdresseIP('192.168.0.0')
    
    assert adresse1.liste_octets() == [192, 168, 0, 1]
    assert adresse1.est_reservee() == False
    assert adresse3.est_reservee() == True
    assert adresse2.adresse_suivante().adresse == '192.168.0.3'
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')