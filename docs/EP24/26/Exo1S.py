def ajoute_dictionnaires(d1, d2):
    for cle in d2:
        if cle in d1:
            d1[cle] += d2[cle]
        else:
            d1[cle] = d2[cle]
    return d1

try:
    assert ajoute_dictionnaires({1: 5, 2: 7}, {2: 9, 3: 11}) == {1: 5, 2: 16, 3: 11}
    assert ajoute_dictionnaires({}, {2: 9, 3: 11}) == {2: 9, 3: 11}
    assert ajoute_dictionnaires({1: 5, 2: 7}, {}) == {1: 5, 2: 7}
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')