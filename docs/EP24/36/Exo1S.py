def recherche(caractere, chaine):
    somme = 0
    for lettre in chaine:
        if lettre == caractere:
            somme += 1
    return somme

try:
    assert recherche('e', "sciences") == 2
    assert recherche('i',"mississippi") == 4
    assert recherche('a',"mississippi") == 0
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')