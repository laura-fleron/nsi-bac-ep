def moyenne(tab):
    somme = 0
    for val in tab:
        somme += val
    return somme / len(tab)

try:
    assert moyenne([1.0]) == 1.0
    assert round(moyenne([1.0, 2.0, 4.0]),2) == 2.33
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')