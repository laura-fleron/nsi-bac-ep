def binaire(a):
    bin_a = str(a%2)
    a = a // 2
    while a != 0 :
        bin_a = str(a%2) + bin_a
        a = a // 2
    return bin_a

try:
    assert binaire(83) == '1010011'
    assert binaire(127) == '1111111'
    assert binaire(0) == '0'
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')