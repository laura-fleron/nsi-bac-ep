def recherche(elt, tab):
    '''
    renvoie l’indice de la première occurrence de
    elt dans tab si elt est dans tab et -1 sinon. 
    '''
    assert tab != [], "le tableau est vide"
    for i in range(len(tab)):
        if tab[i] == elt:
            return i        
    return -1  

try:
    assert recherche(1, [2, 3, 4]) == -1
    assert recherche(1, [10, 12, 1, 56]) == 2
    assert recherche(50, [1, 50, 1]) == 1
    assert recherche(15, [8, 9, 10, 15]) == 3
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

