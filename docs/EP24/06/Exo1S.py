def verifie(tab):
    for i in range(1, len(tab)):
        if tab[i] < tab[i-1]:
            return False
    return True

try:
    assert verifie([0, 5, 8, 8, 9]) == True
    assert verifie([8, 12, 4]) == False
    assert verifie([-1, 4]) == True
    assert verifie([5]) == True
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')