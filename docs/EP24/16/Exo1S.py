def ecriture_binaire_entier_positif(n):
    # cas particulier pour n = 0
    if n == 0:
        return '0'
    # cas général
    b = ''
    while n != 0:
        b = str(n % 2) + b
        n = n // 2
    return b

try:
    assert ecriture_binaire_entier_positif(0) == '0'
    assert ecriture_binaire_entier_positif(2) == '10'
    assert ecriture_binaire_entier_positif(105) == '1101001'
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
