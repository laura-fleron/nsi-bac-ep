def echange(tab, i, j):
    '''Echange les éléments d'indice i et j dans le tableau tab.'''
    temp = tab[i]
    tab[i] = tab[j] 
    tab[j] = temp 

def tri_bulles(tab):
    '''Trie le tableau tab dans l'ordre croissant
    par la méthode du tri à bulles.'''
    n = len(tab)
    for i in range(n-1,-1,-1): 
        for j in range(i): 
            if tab[j] > tab[j+1]: 
                echange(tab, j, j+1)

try:
    tab = []
    tri_bulles(tab)
    tab == []

    tab2 = [9, 3, 7, 2, 3, 1, 6]
    tri_bulles(tab2)
    assert tab2 == [1, 2, 3, 3, 6, 7, 9]

    tab3 = [9, 7, 4, 3]
    tri_bulles(tab3)
    assert tab3 == [3, 4, 7, 9]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
