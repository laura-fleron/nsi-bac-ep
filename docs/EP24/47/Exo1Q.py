#Mettre votre code ici

def max_dico(dico):
        ...


# Testez votre algorithme
try:
    assert max_dico({'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}) == ('Ada', 201)
    assert max_dico({'Alan': 222, 'Ada': 201, 'Eve': 220, 'Tim': 50}) == ('Alan', 222)
    print('Tout semble correct 👍')
    
except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')