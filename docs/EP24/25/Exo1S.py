def recherche_min(tab):
    indice_min = 0
    for i in range(len(tab)):
        if tab[i] < tab[indice_min]:
            indice_min = i
    return indice_min

try:
    assert recherche_min([5]) == 0
    assert recherche_min([2, 4, 1]) == 2
    assert recherche_min([5, 3, 2, 2, 4]) == 2
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')