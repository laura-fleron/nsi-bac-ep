def moyenne(tab):
    somme = 0
    coeffs = 0
    for couple in tab:
        somme += couple[0] * couple[1]
        coeffs += couple[1]
    if coeffs == 0:
        return None
    return somme / coeffs

try:
    assert round(moyenne([(8, 2), (12, 0), (13.5, 1), (5, 0.5)]),8) == 9.14285714
    assert moyenne([(3, 0), (5, 0)]) == None
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')