def somme_max(tab):
    n = len(tab)
    sommes_max = [0]*n
    sommes_max[0] = tab[0]
    # on calcule la plus grande somme se terminant en i
    for i in range(1,n):
        if tab[i-1] + tab[i] >= sommes_max[i]: 
            sommes_max[i] = sommes_max[i-1] + tab[i] 
        else:
            sommes_max[i] = 0
    # on en déduit la plus grande somme de celles-ci
    maximum = 0
    for i in range(1, n):
        if sommes_max[i] > maximum :
            maximum = i
    return sommes_max[maximum]

try:
    assert somme_max([1, 2, 3, 4, 5]) == 15
    assert somme_max([1, 2, -3, 4, 5]) == 9
    assert somme_max([1, 2, -2, 4, 5]) == 10
    assert somme_max([1, -2, 3, 10, -4, 7, 2, -5]) == 18
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')
