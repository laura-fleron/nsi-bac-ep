def parcours_largeur(abr):
    chemin = []
    file = [abr]
    
    while len(file) != 0:
        noeud = file.pop(0)
        chemin.append(noeud[1])
        
        if noeud[0] is not None:
            file.append(noeud[0])
        if noeud[2] is not None:
            file.append(noeud[2])
    return chemin
try:
    arbre = ( ( (None, 1, None), 2, (None, 3, None) ), 4, ( (None, 5, None), 6, (None, 7, None) ) )
    assert parcours_largeur(arbre) == [4, 2, 6, 1, 3, 5, 7]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')