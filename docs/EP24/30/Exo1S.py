def fusion(tab1, tab2):
    tab_fusion = []
    i1 = 0
    i2 = 0
    while i1 < len(tab1) and i2 < len(tab2):
        if tab1[i1] < tab2[i2]:
            tab_fusion.append(tab1[i1])
            i1 += 1
        else:
            tab_fusion.append(tab2[i2])
            i2 += 1

    if i1 == len(tab1):
        while i2 < len(tab2):
            tab_fusion.append(tab2[i2])
            i2 += 1
    else:
        while i1 < len(tab1):
            tab_fusion.append(tab1[i1])
            i1 += 1        

    return tab_fusion

try:
    assert fusion([3, 5], [2, 5]) == [2, 3, 5, 5]
    assert fusion([-2, 4], [-3, 5, 10]) == [-3, -2, 4, 5, 10]
    assert fusion([4], [2, 6]) == [2, 4, 6]
    assert fusion([], []) == []
    assert fusion([1, 2, 3], []) == [1, 2, 3]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')