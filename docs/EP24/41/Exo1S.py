class Noeud:
    def __init__(self, etiquette, gauche, droit):
        self.v = etiquette
        self.gauche = gauche
        self.droit = droit

def taille(a):
    if a is None:
        return 0
    else:
        return 1 + taille(a.gauche) + taille(a.droit)

def hauteur(a):
    if a is None:
        return -1
    else:
        return 1 + max(hauteur(a.gauche), hauteur(a.droit))

a = Noeud(1, Noeud(4, None, None),
             Noeud(0, None,
                      Noeud(7, None, None)))

try:
    assert hauteur(a) == 2
    assert taille(a) == 4
    assert hauteur(None) == -1
    assert taille(None) == 0
    assert hauteur(Noeud(1, None, None)) == 0
    assert taille(Noeud(1, None, None)) == 1
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')