def ajoute(indice, element, liste):
    nbre_elts = len(liste)
    L = [0 for i in range(nbre_elts + 1)]
    if indice < nbre_elts:
        for i in range(indice):
            L[i] = liste[i]
        L[indice] = element
        for i in range(indice + 1, nbre_elts + 1):
            L[i] = liste[i-1]
    else:
        for i in range(nbre_elts):
            L[i] = liste[i]
        L[nbre_elts] = element 
    return L

try:
    assert ajoute(1, 4, [7, 8, 9]) == [7, 4, 8, 9]
    assert ajoute(3, 4, [7, 8, 9]) == [7, 8, 9, 4]
    assert ajoute(0, 4, [7, 8, 9]) == [4, 7, 8, 9]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')