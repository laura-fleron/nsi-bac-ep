def liste_puissances(a,n):
    puissances = [a]
    for i in range(n-1):
        puissances.append(puissances[-1] * a)
    return puissances

def liste_puissances_borne(a, borne):
    lst = []
    val = a
    while val < borne:
        lst.append(val)
        val = val * a
    return lst

try:
    assert liste_puissances(3, 5) == [3, 9, 27, 81, 243]
    assert liste_puissances(-2, 4) == [-2, 4, -8, 16]
    assert liste_puissances_borne(2, 16) == [2, 4, 8]
    assert liste_puissances_borne(2, 17) == [2, 4, 8, 16]
    assert liste_puissances_borne(5, 5) == []
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')