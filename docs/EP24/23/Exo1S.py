def insertion_abr(a, cle):
    if a == None:
        return (None, cle, None)
    else:
        valeur = a[1]
        if cle > valeur:
            return (a[0], valeur, insertion_abr(a[2], cle))
        elif cle < valeur:
            return (insertion_abr(a[0], cle), valeur, a[2])
        return a

n0 = (None,0, None)
n3 = (None,3, None)
n2 = (None,2, n3)
abr1 = (n0, 1, n2)

try:
    assert insertion_abr(abr1, 4) == ((None,0,None),1,(None,2,(None,3,(None,4,None))))
    assert insertion_abr(abr1, -5) == (((None,-5,None),0,None),1,(None,2,(None,3,None)))
    assert insertion_abr(abr1, 2) == ((None,0,None),1,(None,2,(None,3,None)))
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')