def rangement_valeurs(notes_eval):
    lst = [0]*11
    for note in notes_eval:
        lst[note] += 1
    return lst

def notes_triees(effectifs_notes):
    triees = []
    for i in range(11):
        if effectifs_notes[i] != 0:
            for _ in range(effectifs_notes[i]):
                triees.append(i)
    return triees

try:
    notes_eval = [2, 0, 5, 9, 6, 9, 10, 5, 7, 9, 9, 5, 0, 9, 6, 5, 4]
    eff =  rangement_valeurs(notes_eval)
    assert eff == [2, 0, 1, 0, 1, 4, 2, 1, 0, 5, 1]
    assert notes_triees(eff) == [0, 0, 2, 4, 5, 5, 5, 5, 6, 6, 7, 9, 9, 9, 9, 9, 10]
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

