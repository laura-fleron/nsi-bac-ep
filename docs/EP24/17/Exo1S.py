def nb_repetitions(elt, tab):
    nb = 0
    for element in tab:
        if element == elt:
            nb += 1
    return nb

try:
    assert nb_repetitions(5, [2, 5, 3, 5, 6, 9, 5]) == 3
    assert nb_repetitions('A', ['B', 'A', 'B', 'A', 'R']) == 2
    assert nb_repetitions(12, [1, '!', 7, 21, 36, 44]) == 0
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

