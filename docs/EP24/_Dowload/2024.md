---
author: Laura Fléron
title: 📖 Ep 2024

tags: 
 - ZIP
---

## téléchargement des sujets

[**:material-progress-download: l'ensemble des sujets en pdf (zippé)**](24-NSI-PF.zip).

[**:material-progress-download: l'ensemble des programmes Python (zippé)**](24-NSI-Python.zip).

