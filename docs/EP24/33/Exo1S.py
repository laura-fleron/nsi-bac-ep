def renverse(mot):
    sol = ''
    for lettre in mot:
        sol = lettre + sol
    return sol

try:
    assert renverse("") == ""
    assert renverse("abc") == "cba"
    assert renverse("informatique") == "euqitamrofni"
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')