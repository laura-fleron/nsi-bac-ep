def min_et_max(tab):
    d = {}
    d['min'] = tab[0]
    d['max'] = tab[0]
    for val in tab:
        if val < d['min']:
            d['min'] = val
        if val > d['max']:
            d['max'] = val
    return d

try:
    assert min_et_max([0, 1, 4, 2, -2, 9, 3, 1, 7, 1]) == {'min': -2, 'max': 9}
    assert min_et_max([0, 1, 2, 3]) == {'min': 0, 'max': 3}
    assert min_et_max([3]) == {'min': 3, 'max': 3}
    assert min_et_max([1, 3, 2, 1, 3]) == {'min': 1, 'max': 3}
    assert min_et_max([-1, -1, -1, -1, -1]) == {'min': -1, 'max': -1}
    print('Tout semble correct 👍')

except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

