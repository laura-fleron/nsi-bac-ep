def correspond(mot, mot_a_trous):
    if len(mot) != len(mot_a_trous):
        return False
    for i in range(len(mot)):
        if mot[i] != mot_a_trous[i] and mot_a_trous[i] != '*':
            return False
    return True

try:
    assert correspond('INFORMATIQUE', 'INFO*MA*IQUE') == True
    assert correspond('AUTOMATIQUE', 'INFO*MA*IQUE') == False
    assert correspond('STOP', 'S*') == False
    assert correspond('AUTO', '*UT*') == True
    print('Tout semble correct 👍')
    
except AssertionError as asser:
    print('Le résultat de votre fonction n\'est pas conforme 🤔')

