---
author: Laura Fléron
title: Difficulté

hide:
  - tags
---
<!--
Cette fiche résume les tags et les associent à un niveau de difficulté.
La note attribuée aux tags est argulentée ci-dessous :
Représ, des nombres
- Min, Max, Moy : Difficulté très faible, attendu minimum en NSI.
- Listes opérations :
- Assertions :
- Dictionnaires : difficulté sup ax listes
- Tris : 
- Récursivité : 
- Dichotomie :
- Algo glouton :
- Poo : Changement de paradigme 
- Prog dynamique :
- Piles, files :
- Boyer Moore : Algo long à fournir
- Arbres B. Rech. : diifculté cumulée par la Poo
- Diviser pour régner : 
- Logique Booléenne :
- KNN :

Ne figurent pas dans la liste :
- Le language SQL
- La programmation dynamique
- Le langage machine
- Boyer Moore
---
-->
🚧 Version actuelle en construction 🚧

Les points proposés ont été fixés par les justifications personnelles données ci-dessus(*).

*Les données ont été importées et n'ont pas été (toutes) vérifiées.*

|Sujet|Rep nbr|Min-Max-Moy|Opé lst|Dict.|Tris|Récur.|Dicho.|Alg glou.|Poo|Prg dyn.|Pile file|A.B.R|Div régner|Log Bool|K N N|Assert|&sum;|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|1|||2|2|3||||||||||||7|
|2||1|||||||||3||||||4|
|3||2|3||||||||||||||5|
|4|||3||||||||||||||3|
|5|||3||||||||||||||3|
|6|||2||||||||||||3||5|
|7|||2|2|3|4|||||||||||11|
|8||1||2|||||4||3||||||10|
|9|||2|||4|||||||4||||10|
|10||1|||||||4||3||||||8|
|11|2||||3||||||||||||5|
|12||||||||3|4|||5|||||12|
|13|||2|||||3|||||||||5|
|14|||2||3||||||||||||5|
|15||1|2||||||||||||||3|
|16|||2|2|||||||||||||4|
|17||1|3||||||||||||||4|
|18||1|3||||||||||||||4|
|19|||3||||||||||||||3|
|20|||3|2|||||||||||||5|
|21|||2|||4|||4|||5|||||15|
|22|||3|2|||||||||||||5|
|23|||3|3||4|||||||||||10|
|24||||3|3||||||||||||6|
|25|||2|||4|||4|||5|||||15|
|26|||2|2|||3||||||||||7|
|27||1|||3||||||||||||4|
|28||1|||||3||||||||||4|
|29|||3|||4|||4|||5|||||16|
|30|2|1|||||||||||||||3|
|31|2||2||||||||||||||4|
|32||1||2|||||4|||||||3|7|
|33||||2|3||||4|||5|||||14|
|34||1|||3||||||||||||4|
|35|||2|||4|||4|||||2|||12|
|36|||3|||4|||||||||||7|
|37||1|||||||4||||||||5|
|38|||2|3|||||||||||||5|
|39|||2|||||||4|||||||6|
|40|||2|||4|||4|||5|||||15|
|41|||2|||4||3|||||||||9|
|42|||2||3||||||||||||5|
|43|2||||5||||||||||||7|
|44|||3||||||||||||||3|
|45|2||||3|4|||||||||||9|
||||||||||||||||||**7**|

(*) Liste des justifications en préparation.